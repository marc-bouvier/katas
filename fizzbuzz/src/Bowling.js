export default class Bowling {
    
    constructor() {
	this.scoreValue = 0;
	this.rollCount = 0;
	this.currentFrameScore = 0;
    }
    
    score () {
	return this.scoreValue;
    }

    roll(pinCount) {
	this.rollCount++;
	if(this.doubleNext == true) {
		this.scoreValue += pinCount;
		this.doubleNext = false;
	}
	this.scoreValue += pinCount;
	
	if(this.rollCount % 2 == 0) {
                if(this.currentFrameScore + pinCount == 10 ) {
                        this.doubleNext = true;
                }
                this.currentFrameScore = 0;
        } else { 
                this.currentFrameScore = pinCount;
        }
    }
}


# Bowling avec javascript et Jest

[Énoncé du kata](http://codingdojo.org/kata/Bowling/)

[Journal du kata](journal.txt)

## Prérequis

* installer nodejs (npm est inclus avec) : https://nodejs.org/en/
* optionnel installer yarn : https://yarnpkg.com

## Installation

Avec Npm

```bash
npm install
```

Avec Yarn

```bash
yarn
```

## Exécution des tests

Avec Npm

```bash
npm test
```

Avec yarn

```bash
yarn test
```

Ou mode "watch"

```bash
npm run test:watch
```

```bash
yarn test:watch
```

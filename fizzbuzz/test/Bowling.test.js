import Bowling from '../src/Bowling';

test ('score devrait retourner "0" pour un jeu sans roll', () => {
  const game = new Bowling();
  expect (game.score ()).toBe (0);
});

test("score devrait être à 0 lorsqu'aucune quille n'est renversée", () => {
    const game = new Bowling();
    game.roll(0);
    expect (game.score ()).toBe (0);  
});

test("Si je renverse 1 quille, j'obtiens un score de 1", () => {
    let game = new Bowling();
    game.roll(1);
    expect(game.score()).toBe(1);
});

test("Si je fais deux lancers et j'échoue à renverser toutes les quilles, j'obtiens comme score le nombre de quilles renversées", () => {
    let game = new Bowling();
    game.roll(1);
    game.roll(4);
    expect(game.score()).toBe(5);
});

test("Si je fais un spare, mon score doit être de 10 plus la valeur du lancer suivant", () => {
  let game = new Bowling();
  game.roll(4);
  game.roll(6);
  game.roll(3);
  expect(game.score()).toBe(16);
});

test("Si je fais deux spare d'affilée, les deux lancer suivant les spares doivent compter double", () => {
  let game = new Bowling();
  //première frame 5 + 5 + 4 = 14
  game.roll(5);
  game.roll(5);
  //seconde frame 4 + 6 + 3 = 13
  game.roll(4);
  game.roll(6);
  //troisème frame 3
  game.roll(3);

  expect(game.score()).toBe(30);
});
